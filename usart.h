
#ifndef USART_H_
#define USART_H_
#define BAUD 2400
#define FCPU 1000000UL
#define val (((FCPU / (BAUD * 16UL))) - 1)

void usart_init(void);

void send_byte(unsigned char);

unsigned char rec_byte(void);

void send_byte_hex(unsigned char);

void send_str(unsigned char *);

void rec_str(unsigned char *,unsigned int);

#endif