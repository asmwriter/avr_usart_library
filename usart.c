#include "usart.h"

#include <avr/io.h>

void usart_init()
{
//even parity,8 bit data
UBRR0H=(unsigned int)val>>8;
//UBRR0H = 0X00;
UBRR0L=(unsigned int)(val&0xFF);;
//UBRR0L = 0X33;
UCSR0C=(1<<UCSZ00)|(1<<UCSZ01);

UCSR0B=(1<<RXEN0)|(1<<TXEN0);
}

void send_byte(unsigned char c)
{
while(!(UCSR0A & (1<<UDRE0)));
UCSR0A |= (1<<TXC0);
UDR0 = c;
loop_until_bit_is_set(UCSR0A,TXC0);
}


void send_byte_hex(unsigned char c)
{
send_str("0x");
if( ( (c>>4) & 15) < 10 )
	send_byte( ((c>>4)&0xFF) + '0');
else
	send_byte( ((c>>4)&0xFF) + 'A'-10);

c=c<<4;

if( ( (c>>4) & 15) < 10 )
	send_byte(((c>>4)&15) + '0');
else
	send_byte(((c>>4)&15) + 'A'-10);
send_str("\r\n");
}

unsigned char rec_byte()
{
	while(!(UCSR0A & (1<<RXC0)));
	return UDR0;
}

void send_str(unsigned char *s)
{
	while(*s)
		{send_byte(*s);
		s++;
		}
		
}

void rec_str(unsigned char *s,unsigned int buf_size)
{
	char c;
	do{
	c=rec_byte();
	*s=c;
	s++;
	buf_size-=1;
	}
	while(c!='\n' || buf_size ==0 );
}

